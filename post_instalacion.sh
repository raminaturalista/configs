#!/bin/bash



# Instalación de nala para manejar la instalación de paquetes

apt install nala



# actualización de paquetes

echo "Actualización de paquetes"

nala upgrade

nala purge gnome-games gnome-software

nala autoremove

echo "Fin de actualización de paquetes"



# Instalación de software básico

echo "Instalación de software básico"

nala install curl flatpak exa htop neofetch unzip



# Instalación de programas

echo "Instalación de programas"

nala install chromium vlc htop gimp inkscape thunderbird r-base r-base-dev



# Instalación de RStudio

echo "Instalación de RStudio"

cd /home/ramiro/Descargas/

wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-2022.07.1-554-amd64.deb

nala install rstudio-2022.07.1-554-amd64.deb

rm *.deb



# Descarga de archivos de flathub

echo "Descarga de archivos flatpak en carpeta /Descargas"

cd /home/ramiro/Descargas

wget https://dl.flathub.org/repo/appstream/org.telegram.desktop.flatpakref

wget https://dl.flathub.org/repo/appstream/com.visualstudio.code.flatpakref

wget https://dl.flathub.org/repo/appstream/com.obsproject.Studio.flatpakref

wget https://dl.flathub.org/repo/appstream/com.valvesoftware.Steam.flatpakref

echo "Instalación de paquetes flatpak"

flatpak install org.telegram.desktop.flatpakref

flatpak install com.visualstudio.code.flatpakref

flatpak install com.obsproject.Studio.flatpakref

flatpak install com.valvesoftware.Steam.flatpakref

rm *.flatpakref

echo "Fin de la instalación de paquetes flatpak"



# Instalación de fuente JetBrains

wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/JetBrainsMono.zip

mkdir /usr/share/fonts/JetBrainsMono

unzip JetBrainsMono.zip -d /usr/share/fonts/JetBrainsMono

rm *.zip



# Instalación del tema Qogir

mkdir /home/ramiro/.themes

nala install gtk2-engines-murrine gtk2-engines-pixbuf

git clone https://github.com/vinceliuice/Qogir-theme

cd Qogir-theme

./install.sh -d /home/ramiro/.themes -l debian 



# Instalación del tema de íconos Papirus

nala install papirus-icon-theme
